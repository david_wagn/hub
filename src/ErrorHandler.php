<?php

class ErrorHandler {

    protected $fileLocation = "error/index";
    protected $errorCode = "";
    protected $errors = [
        "404"=>"File not found",
    ];

    public function __construct($error) {
        $this->errorCode = $error;
        $errorName = $this->getErrorName($error);
        include "view/" . $this->fileLocation . ".php";
    }

    public function getErrorName($code) {
        return $this->errors[$code];
    }

}