<?php

class tutorials {

    public function index() {
        $view = new View();
        $view->render("tutorials/index", ["title" => "Tutorials &bull; Skip", "search" => true]);
    }

    public function view($id = 0) {
        $view = new View();
        $view->render("tutorials/view", ["title"=> "Tutorials &bull; Skip", "id" => $id]);
    }
}