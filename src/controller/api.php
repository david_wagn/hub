<?php

class api {

    public function id() {
        echo "<span style='font-family: sans-serif; letter-spacing: 3px'>" . ID::generateRandomString(9) . "</span>";
    }

    public function markdown($id = 0) {
        $parsedown = new Parsedown();
        $parsedown->setUrlsLinked(true);
        $parsedown->setSafeMode(false);
        $parsedown->setMarkupEscaped(true);
        echo "<link href='https://fonts.googleapis.com/css2?family=Lato&display=swap' rel='stylesheet'>";
        echo "<style>body {font-family: 'Lato', sans-serif;font-size: 14px;line-height: 1.42857143;}</style>";
        echo $parsedown->parse(file_get_contents(Config::get("assets/path") . $id . "/content.md"));
    }

}