<?php

class content {

    public function index() {
        Redirect::to("/content/vr");
    }

    public function vr($id = 0) {
        $view = new View();
        $view->render("content/vr", ["title" => "VR Inhalt &bull; Skip", "id" => $id, "search" => $id === 0 ? true : false]);
    }

    public function ar($id = 0) {
        $view = new View();
        $view->render("content/ar", ["title" => "AR Inhalt &bull; Skip", "id" => $id, "search" => $id === 0 ? true : false]);
    }

    public function download($id = 0) {
        $view = new View();
        $view->soloRender("content/download", ["title" => "Download &bull; Skip", "id" => $id], true);
    }

    public function render($id = 0) {
        $view = new View();
        $view->soloRender("render/index", ["title" => "Realtime AR &bull; Skip", "id" => $id], true);
    }

}