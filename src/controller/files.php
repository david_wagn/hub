<?php

class files {

    public function index() {
        $view = new View();
        $view->render("files/index", ["title"=>"Dateien &bull; Skip", "search"=>true]);
    }

    public function download($file = "0") {
        $view = new View();
        $view->soloRender("files/download", ["file"=>$file], true);
    }
}