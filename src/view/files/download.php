<?php

if(file_exists(Config::get("assets/path") . "_share/" . $data["file"])) {
    $readableStream = fopen(Config::get("assets/path") . "_share/" . $data["file"], 'rb');
    $writableStream = fopen('php://output', 'wb');

    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . $data["file"] . '"');
    stream_copy_to_stream($readableStream, $writableStream);
    ob_flush();
    flush();
} else {
    echo "File not found";
}