<?php
function renderFile($file) {
    $preview = "";
    if (pathinfo(Config::get("assets/path") . "_share/" . $file, PATHINFO_EXTENSION) == "png" || pathinfo(Config::get("assets/path") . "_share/" . $file, PATHINFO_EXTENSION) == "jpg") {
        $preview = "<div style=\"background: url('" . Config::get("assets/url") . "_share/" . $file . "') no-repeat center center;\" class=\"mx-auto h-64 bg-gray-200 rounded-md\"></div>";
    } else {
        $preview = "<span class='font-bold text-gray-700'>Keine Vorschau verfügbar</span>";
    }
    echo '
<div class="bg-white shadow-lg rounded-lg px-4 py-6 mx-4 my-4">
' . $preview . '
<div class="h-4 mt-2 block mx-auto rounded-sm">' . $file . '</div>
<div class="flex justify-center mt-4">
<a href="/files/download/' . $file . '" target="_blank" class="font-bold text-white rounded-sm px-4 bg-green-300">Download</a>
</div>
</div>
';
} ?>

<div class="spacer-30"></div>
<div class="container px-5 mx-auto">
    <div class="mx-auto text-center">
        <?php if ($handle = opendir(Config::get("assets/path") . "/_share")) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    if (isset($_GET["q"]) && $_GET["q"] != "") {
                        if (strpos(strtolower($entry), strtolower($_GET["q"])) !== false) {
                            renderFile($entry);
                        }
                    } else {
                        renderFile($entry);
                    }
                }
            }
            closedir($handle);
        } ?>
    </div>
</div>