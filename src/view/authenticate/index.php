<?php

if (isset($_SESSION["tokenid"])) {
    Redirect::to("/dashboard");
} else {
    if (Input::exists()) {
        $validate = new Validation();
        $validation = $validate->check($_POST, array(
            "token" => array(
                "displayname" => "Token",
                "required" => true,
                "errorid" => 1,
                "min" => 3,
                "max" => 30,
            ),
        ));

        if ($validation->passed()) {
            $db = DB::getInstance();
            $db->get("authtoken", array("token", "=", hash("sha512", $_POST["token"])));
            if ($db->first()) {
                $_SESSION["tokenid"] = $db->first()["id"];
                Redirect::to("/dashboard");
            } else {
                Redirect::to("/authenticate?invalid");
            }
            $_SESSION["tokenid"];
        }
    }
} ?>
<div class="container mx-auto text-center items-center justify-center">
    <div class="spacer-30"></div>
    <?php if (isset($_GET["invalid"])) { ?>
        <span class="text-lg text-red-600 bg-gray-300 rounded px-2">Invalid token</span>
        <div class="spacer-10"></div>
    <?php } ?>
    <span class="text-lg">Please provide an authentication-token</span>
    <div class="spacer-30"></div>
    <form class="w-lg mx-auto" style="width: 50%" method="post">
        <div class="flex items-center border-b border-b-2 border-indigo-500 py-2">
            <input class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
                   type="password" name="token" placeholder="Token">
            <button class="flex-shrink-0 bg-indigo-500 hover:bg-indigo-500 border-indigo-500 hover:border-indigo-500 text-sm border-4 text-white py-1 px-2 rounded"
                    type="submit">
                Authenticate
            </button>
            <a class="flex-shrink-0 border-transparent border-4 text-indigo-500 hover:text-teal-800 text-sm py-1 px-2 rounded"
               href="/">
                Cancel
            </a>
        </div>
    </form>
</div>

