<?php

if (isset($data["id"]) && $data["id"] != "0") {
    $db = DB::getInstance();
    $db->get("content", array("id", "=", $data["id"]));
    if ($db->first()) {
        $file = Config::get("assets/path") . $data["id"] . "/content.zip";
        if (file_exists($file) && is_readable($file) && preg_match('/\.zip$/', $file)) {
            header('Content-Description: File Transfer');
            header('Content-type: application/zip');
            header("Content-Type: application/force-download");
            header("Content-Disposition: attachment; filename=" . $data["id"] . ".zip");
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
        } else {
            error($file);
        }
    } else {
        error($file);
    }
} else {
    error($file);
}

function error($file) {
    $ff = array_slice(explode("/", $file), -2, 2, false);
    header("HTTP/1.0 404 Not Found");
    print "Error 404: File Not Found: <u>$ff[0]/$ff[1];</u><br>";
    header('Refresh: 5; url=/');
    print 'You will be redirected in 5 seconds';
    exit;
}