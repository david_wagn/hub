<?php
function getImage($id) {
    return file_exists(Config::get("assets/path") . $id . "/thumbnail.png") ? Config::get("assets/url") . $id . "/thumbnail.png" : Config::get("assets/url") . "default.png";
}

function renderContent($data) {
    $tags = "";
    foreach (explode(";", $data["tags"]) as $tag) {
        $tags .= '<a href="?q=' . $tag . '" class="inline-block bg-gray-200 rounded-full px-4 py-1 text-sm font-semibold text-gray-700">' . $tag . '</a>';
    }
    echo '<div class="p-4 md:w-1/2">
<div class="border-2 border-gray-200 rounded-lg overflow-hidden">
<img class="lg:h-48 md:h-36 w-full object-cover object-center" src="' . getImage($data["id"]) . '" alt="Preview">
<div class="p-6">
<h2 class="tracking-widest text-xs title-font font-medium text-gray-500 mb-4">' . $tags . '</h2>
<h1 class="title-font text-lg font-medium text-gray-900 mb-3">' . $data["title"] . '</h1>
<p class="leading-relaxed mb-3">' . $data["description"] . '</p>
<div class="flex items-center flex-wrap ">
<a href="/content/ar/' . $data["id"] . '"
class="text-indigo-500 inline-flex items-center md:mb-2 lg:mb-0">Anschauen&nbsp;
<i class="fas fa-arrow-right"></i></a>
<span class="text-gray-600 mr-0 inline-flex items-center ml-auto leading-none text-sm pr-0 py-0 border-gray-300">' . $data["author"] . '</span>
</div>
</div>
</div>
</div>';
} ?>

<div class="container px-5 mx-auto">
    <?php if ($data["id"] != "0") {
        $db = DB::getInstance();
        $db->get("content", array("id", "=", $data["id"]));
        if ($db->first()) {
            if ($db->first()["type"] == 0) {
                Redirect::to("/content/ar/" . $data["id"]);
            }
            $result = $db->first();
            $tags = "";
            foreach (explode(";", $result["tags"]) as $tag) {
                $tags .= '<a href="?q=' . $tag . '" class="inline-block bg-gray-200 rounded-full px-4 py-1 text-sm font-semibold text-gray-700">' . $tag . '</a>';
            }
        } else {
            Redirect::to("/content/vr");
        } ?>
        <section class="text-gray-700 body-font overflow-hidden">
            <div class="container px-2 py-24 mx-auto">
                <div class="lg:w-4/5 mx-auto flex flex-wrap">
                    <div class="lg:w-1/2 w-full lg:pr-10 lg:py-6 mb-6 lg:mb-0">
                        <h2 class="text-sm title-font text-gray-500 tracking-widest"><?= $tags ?></h2>
                        <h1 class="text-gray-900 text-3xl title-font font-medium mb-4"><?= $result["title"] ?></h1>
                        <div class="flex mb-4">
                            <a class="flex-grow border-b-2 border-gray-300 py-2 text-lg px-1">Beschreibung</a>
                        </div>
                        <p class="leading-relaxed mb-4"><?= $result["description"] ?></p>
                        <div class="flex border-b mb-6 border-gray-300 py-2">
                            <span class="text-gray-500">Erstellt von <?= $result["author"] ?></span>
                        </div>
                        <div class="flex">
                            <a href="/content/render/<?= $result["id"] ?>" target="_blank"
                               class="flex ml-auto text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded">
                                Im Browser starten
                            </a>
                        </div>
                    </div>
                    <img alt="preview" class="lg:w-1/2 w-full lg:h-auto h-64 object-cover object-center rounded"
                         src="<?= getImage($result["id"]); ?>">
                </div>
            </div>
        </section>
        <?php
    } else { ?>
        <section class="text-gray-700 body-font">
            <div class="container px-2 py-24">
                <div class="flex flex-wrap -m-4">
                    <?php $db = DB::getInstance();
                    $db->selectAll("content");
                    foreach ($db->results() as $result) {
                        if (isset($_GET["q"]) && $_GET["q"] != "") {
                            if (strpos(strtolower($result["title"]), strtolower($_GET["q"])) !== false || strpos(strtolower($result["tags"]), strtolower($_GET["q"])) !== false) {
                                if ($result["type"] == 1) {
                                    renderContent($result);
                                }
                            }
                        } else {
                            if ($result["type"] == 1) {
                                renderContent($result);
                            }
                        }
                    } ?> </div>
        </section>
    <?php } ?> </div>