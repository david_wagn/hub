<?php
$db = DB::getInstance();
$db->get("content", array("id", "=", $data["id"]));
if (!$db->first()) {
    Redirect::to("/content/ar");
}

?>
<title><?= $data["title"] ?></title>
<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

<!-- Asset-Based imports -->
<script src="<?= Config::get("assets/url") . "_source/js/three.js" ?>"></script>
<script src="<?= Config::get("assets/url") . "_source/js/three.module.js" ?>"></script>
<script src="<?= Config::get("assets/url") . "_source/js/OBJLoader.js" ?>"></script>
<script src="<?= Config::get("assets/url") . "_source/js/MTLLoader.js" ?>"></script>
<script src="<?= Config::get("assets/url") . "_source/js/MTLLoader.js" ?>"></script>

<script src="<?= Config::get("assets/url") . "_source/js/artoolkit.min.js" ?>"></script>
<script src="<?= Config::get("assets/url") . "_source/js/artoolkit.api.js" ?>"></script>

<script src="<?= Config::get("assets/url") . "_source/js/threex/threex-artoolkitsource.js" ?>"></script>
<script src="<?= Config::get("assets/url") . "_source/js/threex/threex-artoolkitcontext.js" ?>"></script>
<script src="<?= Config::get("assets/url") . "_source/js/threex/threex-arbasecontrols.js" ?>"></script>
<script src="<?= Config::get("assets/url") . "_source/js/threex/threex-armarkercontrols.js" ?>"></script>

<script src="<?= Config::get("assets/url") . "_source/js/jquery.min.js" ?>" crossorigin="anonymous"></script>
<script src="<?= Config::get("assets/url") . "_source/js/microcache.js" ?>" crossorigin="anonymous"></script>
<script src="<?= Config::get("assets/url") . "_source/js/Stats.js" ?>"></script>

<link rel="stylesheet" href="<?= Config::get("assets/url") . "_source/css/bootstrap.min.css" ?>">

<!-- CDN-Based imports -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
      integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

<!-- Disable mobile zoom -->
<style>
    :root {
        touch-action: pan-x pan-y;
        height: 100%
    }
</style>

<body style="margin: 0; overflow: hidden; background-color: #000; z-index: 90">
<div id="loader-container">
    <div class="progress" style="background-color: #000">
        <div id="loader" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
             aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
    <div style="display: flex; justify-content: center; align-content: center;">
        <h1 class="text-white text-center" id="info" style="margin-top: 20%"></h1>
    </div>
</div>
<div class="btn-group" style="z-index: 99; position: fixed; bottom: 20px; width: 100%; padding: 10px;">
    <button class="btn btn-primary p-0 m-1" style="height: 30px; width: 100%;"
            onclick="updateMeshRotation(rotation+rotationfactor)"><i class="fas fa-undo"></i></button>
    <br>
    <button class="btn btn-primary p-0 m-1" style="height: 30px; width: 100%;"
            onclick="updateMeshRotation(rotation-rotationfactor)"><i class="fas fa-redo"></i></button>
    <br>
    <button class="btn btn-primary p-0 m-1" style="height: 30px; width: 100%;"
            onclick="updateMeshScale(scale+scalefactor)"><i class="fas fa-angle-double-up"></i></button>
    <br>
    <button class="btn btn-primary p-0 m-1" style="height: 30px; width: 100%;"
            onclick="updateMeshScale(scale-scalefactor)"><i class="fas fa-angle-double-down"></i></button>
</div>

<script>

    /*
    * Content renderer
    * Realtime media device augmented reality
    * Made for Skip (https://skip-future.de)
    *
    * (c) 2020 David Wagner
    *
    * */

    /* Config */
    let assetUrl = "https://assets.skip-future.de/";
    let tracker = "<?=Config::get("assets/url") . "_data/skip.patt"?>";

    /* Default mesh rotation and scale */
    let rotation = 0;
    let scale = 0.5;
    let mesh;

    let rotationfactor = 3;
    let scalefactor = 0.05;

    let progress = 0;
    let info = "";

    let scene, camera, renderer, clock, deltaTime, totalTime;
    let arToolkitSource, arToolkitContext;
    let markerRoot;

    let stats;

    initialize();
    render();

    function setProgress(int) {
        $("#info").html(int + "% " + this.info);
    }

    function setInfo(text) {
        this.info = text;
    }

    function initialize() {
        setInfo("Initializing scene");
        THREE.Cache.enabled = true;
        scene = new THREE.Scene();
        camera = new THREE.Camera();
        scene.add(camera);

        scene.add(new THREE.AmbientLight(0xcccccc, 1.0));

        setInfo("Initializing renderer");
        renderer = new THREE.WebGLRenderer({
            antialias: true,
            alpha: true
        });

        renderer.setClearColor(new THREE.Color("white"), 0);
        renderer.setSize(640, 480);
        renderer.domElement.style.position = "absolute";
        renderer.domElement.style.top = "0px";
        renderer.domElement.style.left = "0px";
        document.body.appendChild(renderer.domElement);

        clock = new THREE.Clock();
        deltaTime = 0;
        totalTime = 0;

        setInfo("Requesting media source");
        arToolkitSource = new THREEx.ArToolkitSource({
            sourceType: "webcam",
        });

        arToolkitSource.init(function onReady() {
            arToolkitSource.onResizeElement();
            arToolkitSource.copyElementSizeTo(renderer.domElement);
            if (arToolkitContext.arController !== null) {
                arToolkitSource.copyElementSizeTo(arToolkitContext.arController.canvas)
            }
        });

        window.addEventListener("resize", function () {
            arToolkitSource.onResizeElement();
            arToolkitSource.copyElementSizeTo(renderer.domElement);
            if (arToolkitContext.arController !== null) {
                arToolkitSource.copyElementSizeTo(arToolkitContext.arController.canvas)
            }
        });

        setInfo("Loading tracking data");
        arToolkitContext = new THREEx.ArToolkitContext({
            cameraParametersUrl: "<?=Config::get("assets/url") . "_data/camera_para.dat"?>",
            detectionMode: "mono"
        });

        arToolkitContext.init(function onCompleted() {
            camera.projectionMatrix.copy(arToolkitContext.getProjectionMatrix());
        });

        markerRoot = new THREE.Group();
        scene.add(markerRoot);

        setInfo("Loading tracker " + tracker);
        new THREEx.ArMarkerControls(arToolkitContext, markerRoot, {
            type: "pattern", patternUrl: tracker,
        });

        function onProgress(xhr) {
            if (document.getElementsByTagName("video")[0] != null) document.getElementsByTagName("video")[0].style.display = "none";
            if (document.getElementsByTagName("canvas")[0] != null) document.getElementsByTagName("canvas")[0].style.display = "none";
            progress = Math.round(xhr.loaded / xhr.total * 100);

            $("#loader").width(progress + "%");
            if (progress > 99) {
                setInfo("Done");
            } else if (progress > 90) {
                setInfo("Finishing");
            } else if (progress > 75) {
                setInfo("Requesting tracking data");
            }

            setProgress(progress);
        }

        function onError(xhr) {
            alert("An unexpected error happened, please report - " + xhr);
        }

        setTimeout(function () {
            setInfo("Loading object <i><?=$data["id"]?></i>");
        }, 3000);

        new THREE.MTLLoader().setPath(assetUrl + "<?=$data["id"]?>/").load("material.mtl", function (materials) {
            materials.preload();
            new THREE.OBJLoader().setMaterials(materials).setPath(assetUrl + "<?=$data["id"]?>/").load("object.obj", function (group) {
                postLoad(group);
            }, onProgress, onError);
        });

        function postLoad(group) {
            window.setTimeout(function () {
                document.getElementsByTagName("video")[0].style.display = "block";
                document.getElementsByTagName("canvas")[0].style.display = "block";
                $("#loader-container").remove();
            }, 100);
            stats = new Stats();
            stats.showPanel(0);
            document.body.appendChild(stats.dom);
            requestAnimationFrame(renderStats);
            group.children.forEach(children => {
                mesh = children;
                mesh.material.side = THREE.DoubleSide;
                mesh.position.y = 0.5;
                mesh.scale.set(scale, scale, scale);
                mesh.rotation.set(0, rotation, 0);
                markerRoot.add(mesh);
            });
        }
    }

    function updateMeshRotation(r) {
        if (mesh != null) {
            rotation = r;
            markerRoot.remove(mesh);
            mesh.rotation.set(0, 0, r);
            markerRoot.add(mesh);
        }
    }

    function updateMeshScale(f) {
        if (mesh != null) {
            scale = f;
            markerRoot.remove(mesh);
            mesh.scale.set(f, f, f);
            markerRoot.add(mesh);
        }
    }

    function render() {
        requestAnimationFrame(render);
        deltaTime = clock.getDelta();
        totalTime += deltaTime;

        if (arToolkitSource.ready !== false)
            arToolkitContext.update(arToolkitSource.domElement);

        renderer.render(scene, camera);
    }

    function renderStats() {
        stats.begin();
        stats.end();
        requestAnimationFrame(renderStats);
    }

</script>
</body>