<?php

function getImage($name) {
    return file_exists(Config::get("assets/path") . "_team/" . $name . ".png") ? Config::get("assets/url") . "_team/" . $name . ".png" : Config::get("assets/url") . "default.png";
}

function renderPerson($displayname, $role, $imagename, $email = "") {
    $email = $email != "" ? "<a href='mailto:" . $email . "' class='text-blue-500'>E-Mail</a>" : "";
    echo '
<div class="p-2 lg:w-1/3 md:w-1/2 w-full">
<div class="flex items-center border-gray-200 border p-4 rounded-lg">
<img alt="team" class="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4"
src="' . getImage($imagename) . '">
<div class="flex-grow">
<h2 class="text-gray-900 title-font font-medium">' . $displayname . '</h2>
<p class="text-gray-500">' . $role . '</p>
' . $email . '
</div>
</div>
</div>';
}

?>
<section class="text-gray-700 body-font">
    <div class="container px-5 py-3 mx-auto">
        <div class="flex flex-col">
            <div class="h-1 bg-gray-200 rounded overflow-hidden">
                <div class="w-24 h-full bg-indigo-500"></div>
            </div>
            <div class="flex flex-wrap sm:flex-row flex-col py-6 mb-12">
                <h1 class="sm:w-2/5 text-gray-900 font-medium title-font text-2xl mb-2 sm:mb-0">Digitale Lerninhalte für
                    den Unterricht</h1>
                <p class="sm:w-3/5 leading-relaxed text-base sm:pl-10 pl-0">Digitale Modelle und Simulationen sorgen für
                    schnelles
                    Verständnis von Themen und komplexen Abläufen</p>
            </div>
        </div>
        <div class="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4">
            <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
                <div class="rounded-lg h-64 overflow-hidden">
                    <img alt="content" class="object-cover object-center h-full"
                         src="<?= Config::get("assets/url") . "_images/virtual-reality.png" ?>">
                </div>
                <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Virtual Reality</h2>
                <p class="text-base leading-relaxed mt-2">Virtuelle Inhalte und Simulationen zum anschauen und
                    interagieren, welche mit einer
                    <a class="underline" target="_blank" href="https://de.wikipedia.org/wiki/Oculus_Rift">Oculus
                        Rift</a> verwendetwerden können.</p>
                <a href="/content/vr" class="text-indigo-500 inline-flex items-center mt-3">Inhalte ansehen
                    &nbsp;<i class="fas fa-arrow-right"></i>
                </a>
            </div>
            <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
                <div class="rounded-lg h-64 overflow-hidden">
                    <img alt="content" class="object-cover object-center h-full"
                         src="<?= Config::get("assets/url") . "_images/augmented-reality.png" ?>">
                </div>
                <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Augmented Reality</h2>
                <p class="text-base leading-relaxed mt-2">Erweiterte Realität lässt sich einfach und unkompliziert mit
                    jedem Gerät im
                    Browser abrufen.</p>
                <a href="/content/ar" class="text-indigo-500 inline-flex items-center mt-3">Inhalte anzeigen
                    &nbsp;<i class="fas fa-arrow-right"></i>
                </a>
            </div>
            <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
                <div class="rounded-lg h-64 overflow-hidden">
                    <img alt="content" class="object-center h-full"
                         src="<?= Config::get("assets/url") . "_images/tutorial.png" ?>">
                </div>
                <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Tutorials</h2>
                <p class="text-base leading-relaxed mt-2">In unseren Tutorials zeigen wir Ihnen, wie Sie
                    problemlos alle unsere Dienste in weniger als 10 Minuten verstehen und benutzen können.</p>
                &nbsp;<a href="/tutorials" class="text-indigo-500 inline-flex items-center mt-3">Tutorials anzeigen
                    &nbsp;<i class="fas fa-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
</section>
<div class="spacer-100"></div>
<section class="text-gray-700 body-font">
    <div class="container px-5 py-16 mx-auto">
        <div class="flex flex-col text-center w-full mb-20">
            <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Unser Team</h1>
            <p class="lg:w-2/3 mx-auto leading-relaxed text-base">Menschen, die Projekte wie dieses möglich machen</p>
        </div>
        <div class="flex flex-wrap -m-5"> <?php
            renderPerson("Markus Sabath", "Betreuender Lehrer & Organisation", "markus_sabath");

            renderPerson("Tobias Stoll", "Content creator", "tobias_stoll");
            renderPerson("Norbert Borza-Buchauer", "Content creator", "norbert_borza-buchauer");
            renderPerson("Lisa Maurer", "?", "lisa_maurer");
            renderPerson("Angelina Fuhr", "?", "angelina_fuhr");
            renderPerson("Marius Schlechte", "Content creator & Customer relationship", "marius_schlechte");
            renderPerson("Peter Preuss", "Content creator", "peter_preus");
            renderPerson("Sebastian Wüst", "Content creator", "sebastian_wuest");
            renderPerson("Fabian Stucky", "Content creator", "fabian_stucky");

            renderPerson("David Wagner", "Entwicklung & Videoproduktion", "david_wagner", "david@dwagner.one");
            ?> </div>
    </div>
</section>
<section class="text-gray-700 body-font">
    <div class="container px-5 py-10 mx-auto">
        <div class="flex flex-col text-center w-full mb-20">
            <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Unsere Erfolge</h1>
            <p class="lg:w-2/3 mx-auto leading-relaxed text-base">Was wir mit unserem Team bereits erreicht haben</p>
        </div>
        <div class="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4">
            <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
                <div class="rounded-lg h-64 overflow-hidden">
                    <img alt="content" class="object-cover object-center h-full w-full"
                         src="<?= Config::get("assets/url") . "_images/ach_01.png" ?>">
                </div>
                <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Digitize your school!</h2>
                <p class="text-base leading-relaxed mt-2">
                    Zusammen haben wir die IGS Kandel zu einem der Gewinner des Wettbewerbs "Digitize your school!" von
                    SAP gemacht.
                    Bei der Preisverleihung auf dem EduAction Bildungsgipfel in Mannheim wurde die Urkunde überreicht.
                    Der Preis ist mit 20.000 € dotiert.
                </p>
            </div>
            <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
                <div class="rounded-lg h-64 overflow-hidden">
                    <img alt="content" class="object-cover object-center h-full w-full"
                         src="<?= Config::get("assets/url") . "_images/ach_04.png" ?>">
                </div>
                <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Erste Lehrerfortbildung</h2>
                <p class="text-base leading-relaxed mt-2">
                    Im Mai 2019 fand die erste Fortbildung der Schülerfirma Skip für Lehrer statt.
                    Die Lehrer waren sehr zufrieden, und die Fortbildung war ein voller Erfolg. Weitere werden folgen.
                </p>
            </div>
            <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
                <div class="rounded-lg h-64 overflow-hidden">
                    <img alt="content" class="object-cover object-center h-full w-full"
                         src="<?= Config::get("assets/url") . "_images/ach_02.png" ?>">
                </div>
                <h2 class="text-xl font-medium title-font text-gray-900 mt-5">EduAction Expertenforum</h2>
                <p class="text-base leading-relaxed mt-2">
                    Wir waren als einzige Schule zu dem Expertenforum 2019 der Metropolregion Rhein-Neckar eingeladen.
                    Anwesend war viel Prominenz aus Politik und Wirtschaft. Auf dem Forum präsentierten wir unsere
                    Schülerfirma Skip, das Konzept zur Digitalisierung an der IGS Kandel. Unser Konzept erfuhr viel
                    Anerkennung und Lob. Besonders beeindrucken fanden die Teilnehmer die Tatsache, dass bei uns auch
                    mal die Rollen vertauscht werden und Schüler Lehrer schulen.
                </p>
            </div>
            <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
                <div class="rounded-lg h-64 overflow-hidden">
                    <img alt="content" class="object-cover object-center h-full w-full"
                         src="<?= Config::get("assets/url") . "_images/ach_03.png" ?>">
                </div>
                <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Sparkassenstiftung</h2>
                <p class="text-base leading-relaxed mt-2">
                    Die IGS Kandel durfte einen Preis von 5.000 € in Empfang nehmen, der von der Sparkasse
                    Germersheim-Kandel
                    gestiftet wurde. Er drückt die Anerkennung für die geleisteten und die geplanten Anstrengungen bei
                    der Digitalisierung der Schule aus.
                </p>
            </div>
            <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
                <div class="rounded-lg h-64 overflow-hidden">
                    <img alt="content" class="object-cover object-center h-full w-full"
                         src="<?= Config::get("assets/url") . "_images/ach_05.png" ?>">
                </div>
                <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Berufswahlsiegel</h2>
                <p class="text-base leading-relaxed mt-2">
                    Im September 2019 durfte unsere Schülerfirma die IGS Kandel beim Berufswahlsiegel vertreten.
                </p>
            </div>
            <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
                <div class="rounded-lg h-64 overflow-hidden">
                    <img alt="content" class="object-cover object-center h-full w-full"
                         src="<?= Config::get("assets/url") . "_images/ach_06.png" ?>">
                </div>
                <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Zweite Lehrerfortbildung</h2>
                <p class="text-base leading-relaxed mt-2">
                    Wir haben erfolgreich unsere zweite Fortbildung zum Thema Greenscreen veranstaltet.
                </p>
            </div>
            <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
                <div class="rounded-lg h-64 overflow-hidden">
                    <img alt="content" class="object-cover object-center h-full w-full"
                         src="<?= Config::get("assets/url") . "_images/ach_07.png" ?>">
                </div>
                <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Tag der offenen Tür</h2>
                <p class="text-base leading-relaxed mt-2">
                    Am Tag der offenen Tür konnten die Besucher mit unserer VR-Brille in die virtuelle Welt eintauchen.
                </p>
            </div>
        </div>
    </div>
</section>
