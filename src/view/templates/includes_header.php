<head>
    <title><?= $data["title"] ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href=" <?=Config::get("assets/url") . "/_images/icon.png" ?>">

    <link rel="stylesheet" href="<?=Config::get("assets/url") . "_source/css/spacers.css"?>">
    <link rel="stylesheet" href="<?=Config::get("assets/url") . "_source/css/main.css"?>">

    <script src="<?=Config::get("assets/url") . "/_source/js/jquery.min.js" ?>"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/1.4.6/tailwind.min.css">
</head>