<div class="spacer-100"></div>
<footer class="h-20">
    <br>
    <div class="text-center">
        Website made with <span class="text-red-700"><i class="fas fa-heart"></i></span> by David Wagner
    </div>
</footer>