<div class="spacer-30"></div>
<div class="container px-5 mx-auto">
    <form method="get">
        <input name="q"
               class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:border-indigo-500 inline"
               id="grid-password" type="text" placeholder="Suchen ...">
        <button type="submit" class="bg-indigo-500 text-white py-2 px-4 rounded inline"><i
                    class="fas fa-search"></i> Suchen
        </button>
        <?php if (isset($_GET["q"]) && $_GET["q"] != "") { ?>
            <button onclick="window.location.href = window.location.href.split('?')[0]"
                    class="bg-red-600 text-white py-2 px-4 rounded inline">
                <i class="fas fa-trash"></i> Suche leeren
            </button>
        <?php } ?>
    </form>
</div>