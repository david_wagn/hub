<html lang="de">
<head>
    <title><?= $data["title"] ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href=" <?= Config::get("assets/url") . "/_images/icon.png" ?>">

    <link rel="stylesheet" href="<?= Config::get("assets/url") . "_source/css/spacers.css" ?>">
    <link rel="stylesheet" href="<?= Config::get("assets/url") . "_source/css/main.css" ?>">

    <script src="<?= Config::get("assets/url") . "/_source/js/jquery.min.js" ?>"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/1.4.6/tailwind.min.css">
</head>
<nav class="flex items-center justify-between flex-wrap bg-indigo-500 p-6">
    <a href="/">
        <div class="flex items-center flex-shrink-0 text-white mr-6 hover:text-white">
            <span class="font-semibold text-2xl tracking-tight">Skip</span>
            <p class="text-sm">&nbsp; to the future</p>
        </div>
    </a>
    <div class="block lg:hidden">
        <button class="flex items-center px-3 py-2 border rounded text-white border-white hover:text-white hover:border-white">
            <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title>
                <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
            </svg>
        </button>
    </div>
    <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
        <div class="text-lg lg:flex-grow">
            <a href="/content/vr" class="block mt-4 lg:inline-block lg:mt-0 text-gray-300 hover:text-white mr-4">
                Virtual Reality
            </a>
            <a href="/content/ar" class="block mt-4 lg:inline-block lg:mt-0 text-gray-300 hover:text-white mr-4">
                Augmented Reality
            </a>
            <a href="/tutorials" class="block mt-4 lg:inline-block lg:mt-0 text-gray-300 hover:text-white mr-4">
                Tutorials
            </a>
            <a href="/files" class="block mt-4 lg:inline-block lg:mt-0 text-gray-300 hover:text-white mr-4">
                Dateien
            </a>
        </div>
    </div>
</nav>