<div class="spacer-100"></div>
<footer class="h-20">
    <br>
    <div class="text-center">
        (v<?= Config::get("app/version") ?>) Website made with <span class="text-red-700"><i
                    class="fas fa-heart"></i></span> by <?= Config::get("app/author") ?>
    </div>
    <div class="text-center">
        <a href="/credits" class="text-blue-500">Credits</a>
    </div>
</footer>