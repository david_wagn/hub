<?php
function getImage($id) {
    return file_exists(Config::get("assets/path") . $id . "/thumbnail.png") ? Config::get("assets/url") . $id . "/thumbnail.png" : Config::get("assets/url") . "default.png";
} ?>
    <div class="container px-5 mx-auto">
<?php if ($data["id"] != "0") {
    $db = DB::getInstance();
    $db->get("tutorials", array("id", "=", $data["id"]));
    if ($db->first()) {
        $result = $db->first();
        $tags = "";
        foreach (explode(";", $result["tags"]) as $tag) {
            $tags .= '<span class="inline-block bg-gray-200 rounded-full px-4 py-1 text-sm font-semibold text-gray-700">' . $tag . '</span>';
        }
    } else {
        Redirect::to("/tutorials");
    } ?>
    <section class="text-gray-700 body-font overflow-hidden">
        <div class="container px-5 py-24 mx-auto">
            <div class="lg:w-4/5 mx-auto flex flex-wrap">
                <div class="lg:w-1/2 w-full lg:pr-10 lg:py-6 mb-6 lg:mb-0">
                    <h2 class="text-sm title-font text-gray-500 tracking-widest"><?= $tags ?></h2>
                    <h1 class="text-gray-900 text-3xl title-font font-medium mb-4"><?= $result["title"] ?></h1>
                    <div class="flex mb-4">
                        <a class="flex-grow border-b-2 border-gray-300 py-2 text-lg px-1">Beschreibung</a>
                    </div>
                    <p class="leading-relaxed mb-4"><?= $result["description"] ?></p>
                    <div class="flex border-b mb-6 border-gray-300 py-2">
                        <span class="text-gray-500">Erstellt von <?= $result["author"] ?></span>
                    </div>
                </div>
                <img alt="preview" class="lg:w-1/2 w-full lg:h-auto h-64 object-cover object-center rounded"
                     src="<?= getImage($result["id"]); ?>">
            </div>
            <div class="spacer-30"></div>
            <?php if (file_exists(Config::get("assets/path") . $result["id"] . "/content.md")) { ?>
                <iframe src="/api/markdown/<?= $result["id"] ?>" width="100%" height="100%"></iframe>
            <?php } else { ?>
                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" role="alert">
                    <strong class="font-bold">Ups!</strong>
                    <span class="block sm:inline">Tutorialinhalt konnte nicht gefunden werden.</span>
                </div>
            <?php } ?>
        </div>
    </section>
    </div>
<?php } else {
    Redirect::to("/tutorials");
} ?>