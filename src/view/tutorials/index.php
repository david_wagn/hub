<?php
function getImage($id) {
    return file_exists("/var/www/private/skip-future-de/assets/" . $id . "/thumbnail.png") ? "https://assets.skip-future.de/" . $id . "/thumbnail.png" : "https://assets.skip-future.de/default.png";
}

function renderContent($data) {
    $tags = "";
    foreach (explode(";", $data["tags"]) as $tag) {
        $tags .= '<a href="?q=' . $tag . '" class="inline-block bg-gray-200 rounded-full px-4 py-1 text-sm font-semibold text-gray-700">' . $tag . '</a>';
    }
    echo '<div class="p-4 md:w-1/2">
<div class="border-2 border-gray-200 rounded-lg overflow-hidden">
<img class="lg:h-48 md:h-36 w-full object-cover object-center" src="' . getImage($data["id"]) . '" alt="Preview">
<div class="p-6">
<h2 class="tracking-widest text-xs title-font font-medium text-gray-500 mb-4">' . $tags . '</h2>
<h1 class="title-font text-lg font-medium text-gray-900 mb-3">' . $data["title"] . '</h1>
<p class="leading-relaxed mb-3">' . $data["description"] . '</p>
<div class="flex items-center flex-wrap ">
<a href="/tutorials/view/' . $data["id"] . '"
class="text-indigo-500 inline-flex items-center md:mb-2 lg:mb-0">Anschauen&nbsp;
<i class="fas fa-arrow-right"></i></a>
<span class="text-gray-600 mr-0 inline-flex items-center ml-auto leading-none text-sm pr-0 py-0 border-gray-300">' . $data["author"] . '</span>
</div>
</div>
</div>
</div>';
} ?>

<div class="container px-5 mx-auto">
    <section class="text-gray-700 body-font">
        <div class="container px-2 py-24">
            <div class="flex flex-wrap -m-4">
                <?php $db = DB::getInstance();
                $db->selectAll("tutorials");
                foreach ($db->results() as $result) {
                    if (isset($_GET["q"]) && $_GET["q"] != "") {
                        if (strpos(strtolower($result["title"]), strtolower($_GET["q"])) !== false || strpos(strtolower($result["tags"]), strtolower($_GET["q"])) !== false) {
                            renderContent($result);
                        }
                    } else {
                        renderContent($result);
                    }
                } ?> </div>
    </section>
</div>