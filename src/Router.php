<?php

class Router {

    public $url;
    protected $controller = "index";
    protected $method = "index";
    protected $params = [];

    public function __construct() {
        $url = $this->parseUrl();

        if (file_exists("../src/controller/" . $url[0] . ".php")) {
            $this->controller = $url[0];
            unset($url[0]);
        } else {
            if (!is_null($url)) {
                new ErrorHandler("404");
                return;
            }
        }

        include_once "../src/controller/" . $this->controller . ".php";
        $this->controller = new $this->controller;

        if (isset($url[1])) {
            if (method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
                unset($url[1]);
            } else {
                new ErrorHandler("404");
                return;
            }
        }

        $this->params = $url ? array_values($url) : [];
        call_user_func_array([$this->controller, $this->method], $this->params);
    }


    public function parseUrl() {
        if (isset($_GET["url"])) {
            return $url = explode("/", filter_var(rtrim($_GET["url"], "/"), FILTER_SANITIZE_URL));
        } else {
            return null;
        }
    }
}