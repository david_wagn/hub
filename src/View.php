<?php

class View {

    public function render($view, $data = ["pagetitle"=>"blank"]) {
        if(file_exists("../src/view/" . $view . ".php")) {
            $this->renderHeader($data);
            if(isset($data["search"]) && $data["search"] == true) {
                include "../src/view/templates/search.php";
            }
            include "../src/view/" . $view . ".php";
            $this->renderFooter($data);
        } else {
            new ErrorHandler("404");
        }
    }

    public function soloRender($view, $data = ["pagetitle"=>"blank"], $noincludes = false) {
        if(file_exists("../src/view/" . $view . ".php")) {
            if($noincludes) {
                include "../src/view/" . $view . ".php";
            } else {
                include "../src/view/templates/includes_header.php";
                include "../src/view/" . $view . ".php";
                include "../src/view/templates/includes_footer.php";
            }
        } else {
            new ErrorHandler("404");
        }
    }

    private function renderHeader($data = ["pagetitle"=>"blank"]) {
        if(file_exists("../src/view/templates/header.php")) {
            include "../src/view/templates/header.php";
        } else {
            new ErrorHandler("404");
        }
    }

    private function renderFooter($data = []) {
        if(file_exists("../src/view/templates/footer.php")) {
            include "../src/view/templates/footer.php";
        } else {
            new ErrorHandler("404");
        }
    }

}