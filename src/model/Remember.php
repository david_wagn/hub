<?php

class Remember {

    public static function rememberUser($formID) {
        setcookie($formID, "true", time() + 60*60*24*7, "/");
    }

    public static function isRemembered($formID) {
        if(isset($_COOKIE[$formID])) {
            if($_COOKIE[$formID] == "true") {
                return true;
            }
        }
        return false;
    }

}