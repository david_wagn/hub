<?php

class Validation {

    private $_passed = false,
        $_errors = array(),
        $_db = null,
        $_errorids = array();

    public function __construct() {
        $this->_db = DB::getInstance();
    }

    public function check($source, $items = array()) {
        foreach ($items as $item => $rules) {
            foreach ($rules as $rule => $rule_value) {
                $value = trim($source[$item]);
                $displayname = $rules["displayname"];
                $errorid = $rules["errorid"];
                $item = htmlspecialchars($item);
                if ($rule === "required" && empty($value)) {
                    $this->addError("{$displayname} wird benötigt", $errorid);
                } else if (!empty($value)) {
                    switch ($rule) {
                        case "min":
                            if (strlen($value) < $rule_value) {
                                $this->addError("{$displayname} muss mindestens {$rule_value} Zeichen lang sein", $errorid);
                            }
                            break;
                        case "max":
                            if (strlen($value) > $rule_value) {
                                $this->addError("{$displayname} darf maximal {$rule_value} Zeichen lang sein", $errorid);
                            }
                            break;
                        case "matches":
                            if ($value != $source[$rule_value]) {
                                $this->addError("{$displayname} muss identisch zu {$items[$rules["matches"]]["displayname"]} sein", $errorid);
                            }
                            break;
                        case "unique":
                            $check = $this->_db->get("{$rule_value}", array($item, "=", $value));
                            if ($check->count()) {
                                $this->addError("{$displayname} {$value} existiert bereits", $errorid);
                            }
                            break;

                    }
                }
            }
        }

        if (empty($this->_errors)) {
            $this->_passed = true;
        }

        return $this;

    }

    private function addError($error, $errorid) {
        $this->_errors[] = $error;
        $this->_errorids[] = $errorid;
    }

    public function errors() {
        return $this->_errors;
    }

    public function errorIds() {
        return $this->errorIds();
    }

    public function passed() {
        return $this->_passed;
    }
}