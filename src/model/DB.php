<?php

class DB {

    private static $_instance = null;
    private $_pdo,
        $_query,
        $_error = false,
        $_result,
        $_count = 0;

    private function __construct() {
        try {
            $this->_pdo = new PDO("mysql:host=" . Config::get("mysql/host") . ";dbname=" . Config::get("mysql/dbname"),
                Config::get("mysql/username"),
                Config::get("mysql/password"));
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public static function getInstance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new DB();
        }
        return self::$_instance;
    }

    public function get($table, $where, $extra = "") {
        return $this->action("SELECT *", $table, $where, $extra);
    }

    public function action($action, $table, $where = array(), $extra = "") {
        if (count($where) == 3) {
            $operators = array("=", ">", "<", "<=", ">=");

            $field = $where[0];
            $operator = $where[1];
            $value = $where[2];

            if (in_array($operator, $operators)) {
                $sql = "{$action} FROM {$table} WHERE {$field} {$operator} ? {$extra}";
                if (!$this->query($sql, array($value))) {
                    return $this;
                }
                return $this;
            }
        }
    }

    public function query($sql, $params = array()) {
        $this->_error = false;
        if ($this->_query = $this->_pdo->prepare($sql)) {
            $x = 1;
            if (count($params)) {
                foreach ($params as $param) {
                    $this->_query->bindValue($x, $param);
                    $x++;
                }
            }
            if ($this->_query->execute()) {
                $this->_result = $this->_query->fetchAll(PDO::FETCH_ASSOC);
                $this->_count = $this->_query->rowCount();
            } else {
                $this->_error = true;
            }
        }
        return $this;
    }

    public function delete($table, $where) {
        return $this->action("DELETE", $table, $where);
    }

    public function selectAll($table, $extra = "") {
        $sql = "SELECT * FROM {$table} {$extra}";
        $this->query($sql);
    }

    public function first() {
        return $this->results()[0];
    }

    public function results()  {
        return $this->_result;
    }

    public function count() {
        return $this->_count;
    }

    public function insert($table, $fields = array()) {
        if (count($fields)) {
            $keys = array_keys($fields);
            $values = null;
            $x = 1;

            foreach ($fields as $field) {
                $values .= "?";

                if ($x < count($fields)) {
                    $values .= ", ";
                }
                $x++;
            }

            $sql = "INSERT INTO {$table} (`" . implode('`, `', $keys) . "`) VALUES ({$values})";

            if (!$this->query($sql, $fields)->error()) {
                return true;
            }
        }
        return false;
    }

    public function error() {
        return $this->_error;
    }

    public function update($table, $key, $id, $fields) {
        $set = "";
        $x = 1;

        foreach ($fields as $name => $value) {
            $set .= "{$name} = ?";
            if ($x < count($fields)) {
                $set .= ",";
            }
            $x++;
        }

        $sql = "UPDATE {$table} SET {$set} WHERE {$key}='{$id}'";

        if ($this->query($sql, $fields)->error()) {
            return true;
        }
        return false;
    }


}