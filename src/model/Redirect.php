<?php

class Redirect {

    public static function to($location = null) {
        if ($location) {
            if (is_numeric($location)) {
                switch ($location) {
                    case 404:
                        header("HTTP/1.0 404 Not found");
                        $view = new View();
                        echo "<script>document.getElementsByTagName('header')[0].innerHTML = '';document.getElementsByTagName('head')[0].innerHTML = '';</script>";
                        $view->render("error/404", []);
                        break;
                }
            }
            header("Location:" . $location);
            exit;
        }
    }

}