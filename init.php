<?php

include_once "src/Application.php";
include_once "src/ErrorHandler.php";
include_once "src/Router.php";
include_once "src/View.php";
include_once "src/ErrorHandler.php";
include_once "src/Config.php";
include_once "src/model/DB.php";
include_once "src/model/Validation.php";
include_once "src/model/Input.php";
include_once "src/model/Remember.php";
include_once "src/model/ID.php";
include_once "src/model/Redirect.php";
include_once "src/vendor/Parsedown.php";

$GLOBALS["config"] = [
    "assets" => [
        "path" => "/var/www/private/skip-future-de/assets/",
        "url" => "https://assets.skip-future.de/",
    ],
    "app" => [
        "version" => "1.4",
        "author" => "David Wagner",
        "debug" => true
    ],
    "mysql" => [
        "host" => "127.0.0.1",
        "username" => "skiphub",
        "password" => "2bD7xzg6IVBjvaiD",
        "dbname" => "skiphub"
    ],
];

session_start();
if (isset($_SESSION["id"])) {
} else $_SESSION["id"] = ID::generateRandomString(7);

if(Config::get("app/debug")) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

DB::getInstance();
new Application();